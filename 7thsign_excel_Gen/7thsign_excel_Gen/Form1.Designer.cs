﻿namespace _7thsign_excel_Gen
{
    partial class ExcelGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExcelGenerator));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Generate_excel = new System.Windows.Forms.Button();
            this.Test_File = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(252, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(722, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Generate_excel
            // 
            this.Generate_excel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Generate_excel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Generate_excel.Location = new System.Drawing.Point(559, 172);
            this.Generate_excel.Name = "Generate_excel";
            this.Generate_excel.Size = new System.Drawing.Size(159, 102);
            this.Generate_excel.TabIndex = 1;
            this.Generate_excel.Text = "Generate excel";
            this.Generate_excel.UseVisualStyleBackColor = false;
            this.Generate_excel.Click += new System.EventHandler(this.Generate_excel_Click);
            // 
            // Test_File
            // 
            this.Test_File.Location = new System.Drawing.Point(1001, 34);
            this.Test_File.Name = "Test_File";
            this.Test_File.Size = new System.Drawing.Size(101, 94);
            this.Test_File.TabIndex = 2;
            this.Test_File.Text = "Test_File";
            this.Test_File.UseVisualStyleBackColor = true;
            this.Test_File.Click += new System.EventHandler(this.button2_Click);
            // 
            // ExcelGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1393, 322);
            this.Controls.Add(this.Test_File);
            this.Controls.Add(this.Generate_excel);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExcelGenerator";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Generate_excel;
        private System.Windows.Forms.Button Test_File;
    }
}

