while(size > 1)
{
    var temp = 0;
    var r = 0;
    for(var xx=0; xx<10;xx++){
        for(var yy=0; yy<10;yy++){
            if(size > 1){//Häng nur solange Räume an, bis du die gewünschte Size erreicht hast
            temp = dungeon[xx,yy];
                if(temp == -1){
                    //An der aktuellen Position ist ein Raum!
                    r = floor(random_range(0,3));
                        if(r == 0){
                            if(yy > 0){//Räume dürfen nicht "out of bounds" platziert werden!
                                if(dungeon[xx,yy-1] == 0){
                                //Über dem aktuellen Raum wird ein neuer "normaler" Raum platziert.
                                dungeon[xx,yy-1] = -1;
                                size--;
                                }
                            }
                        }
                    else if(r == 1){
                        if(xx < 9){//Räume dürfen nicht "out of bounds" platziert werden!
                            if(dungeon[xx+1,yy] == 0){
                                //Rechts vom aktuellen Raum wird ein neuer "normaler" Raum platziert.
                                dungeon[xx+1,yy] = -1;
                                size--;
                            }
                        }
                    }
                    else if(r == 2){
                        if(yy < 9){//Räume dürfen nicht "out of bounds" platziert werden!
                            if(dungeon[xx,yy+1] == 0){
                                //Unter dem aktuellen Raum wird ein neuer "normaler" Raum platziert.
                                dungeon[xx,yy+1] = -1;
                                size--;
                            }
                        }
                    }
                    else if(r == 3){
                        if(xx > 0){//Räume dürfen nicht "out of bounds" platziert werden!
                            if(dungeon[xx-1,yy] == 0){
                                //Links vom aktuellen Raum wird ein neuer "normaler" Raum platziert.
                                dungeon[xx-1,yy] = -1;
                                size--;
                            }
                        }
                    }
                }
            }
        }
    }
}
